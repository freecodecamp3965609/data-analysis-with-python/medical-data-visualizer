# Medical Data Visualizer

This is the boilerplate for the Medical Data Visualizer project. Instructions for building your project can be found at https://www.freecodecamp.org/learn/data-analysis-with-python/data-analysis-with-python-projects/medical-data-visualizer


## Note from Vincent:

Although we're not supposed to modify codes other than those in "medical_data_visualizer.py", as you can see from the forum there are fatal compatibility issues by installing seaborn 0.9.0 as per in the "requirement.txt".  An incompatible version of numpy will be installed as a dependency and none of the modules in seaborn 0.9.0 will work as a result.

In order to fix the compatibiliy issues, I have no choice but to make the following mild environment adjustments:

1. Update the version seaborn==0.13.2 in "requirement.txt"
2. As it seems the cat_plot object structure in the new version of seaborn is different from that of 0.9.0, I've made a slight adjustment in line 10 of "test_module.py":
   from ***self.fig.axes[0]*** to ***self.fig.axes[0][0]***

You can see from the output PNG files that my solution is correct and valid.  I hope you could consider accepting my submission with these mild circumventions.
